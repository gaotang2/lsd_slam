#!/usr/bin/python
"""
In this file, I dump the camera calibration data into json file which is readable by opencv
"""
from dicttoxml import dicttoxml

data = dict(Camera_Matrix = [[1459.350261293599, 0.0, 936.395832784679], [0.0, 1454.523249372403, 693.0275372846803], [0.0, 0.0, 1.0]], Distortion_Coefficients = [-0.06806374181729541, 1.727943254181413, -0.004167575721891483, -0.009607071635809723, 0.4727304262201982, -0.2948815842502582, 0.9950022654854973, 0.4526836378939169], image_Width=1920, image_Height=1440)
xmldata = dicttoxml(data)
with open('ros_camera.xml', 'wt') as f:
    f.write(xmldata)
